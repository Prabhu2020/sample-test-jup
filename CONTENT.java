package com.alation.github.http;

public enum CONTENT {

	CSV("text/csv", false, new String[] {"csv"}), 
	XML("text/xml", false, new String[] {"xml"}),
	TXT("text/plain", false, new String[] {"txt"}), 
	DIRECTORY("application/x-directory", true, null), // Directory does not have any extensions
	JSON("application/json", false, new String[] {"json"} ), 
	ZIP("application/zip", false, new String[] {"zip"} ),
	JAR("application/java-archive", false, new String[] {"jar"} ),
	TAR("application/x-tar", false, new String[] {"tar"}),
	GZIP("application/x-gzip", false, new String[] { "gz" }),
	DOC("application/msword", false, new String[] { "doc" }),
	IMAGE("image/jpg", false, new String[] { "png", "jpeg", "jpg" }),
	PDF("application/pdf", false, new String[] { "pdf" }),
	JPY("application/ipynb", false, new String[] { "ipynb" }),
	OCTET_STREAM("binary/octet-stream", false, new String[] { "avro", "java","c","cpp","py","js" ,"php","net","sql","vbs","au3","cmd","bat","ksh","sh","pl"}), // avro
	APPLICATION_OCTET_STREAM("application/octet-stream", false, new String[] { "parquet", "orc", "xlxs",
			"pptx", "docx", "vsd", "pub" }), // parquet, orc and microsoft files
	UNKNOWN("application/xyz", true, null);
	
	private final String type;
	private final boolean directory;
	private final String[] extensions;

	CONTENT(String type, boolean directory, String [] extensions) {
		this.type = type;
		this.directory = directory;
		this.extensions = extensions;
	}

	public static CONTENT search(String contentType) {
		for (CONTENT content : CONTENT.values()) {
			if (content.type.equalsIgnoreCase(contentType)) {
				return content;
			}
		}

		System.err.println(">>>>> Unable to locate Content type=" + contentType + " Returning UNKNOWN");

		// Return null
		return CONTENT.UNKNOWN;
	}
	
	
	public static CONTENT searchByExtension(String extension) {
		for (CONTENT content : CONTENT.values()) {
			if(content.supportsExtension(extension)) {
				return content;
			}
		}
		
		System.err.println(">>>>> Unable to locate extension=" + extension + " Returning UNKNOWN");
		
		// Unanle to locate
		return CONTENT.UNKNOWN;
	}

	public String getType() {
		return type;
	}
	
	public boolean isDirectory() {
		return directory;
	}

	public String[] getExtensions() {
		return extensions;
	}
	
	/**
	 * Method returns <code>true</code> if incoming file extension supported by this content.
	 * @param ext - String file extension you are trying to match.
	 * @return boolean value - true - if this content supports incoming extension type.
	 */
	public boolean supportsExtension(String ext) {
		if(extensions != null) {
			for (int i = 0; i < extensions.length; i++) {
				if(ext.equalsIgnoreCase(extensions[i])) {
					return true;
				}
			}
		}
		return false;
	}
}
