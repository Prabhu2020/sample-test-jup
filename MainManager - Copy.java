package com.alation.tableau;

import api.utils.APIUtils;

import com.alation.tableau.gbm.GBMObjectStore;
import com.alation.tableau.providers.*;
import com.alation.tableau.utils.ApiUtils;
import com.alation.tableau.utils.Constants;
import com.alation.tableau.utils.PropHelper;
import com.alation.tableau.utils.TableauConstants;
import com.alation.tableau.utils.TableauUtil;
import com.fasterxml.jackson.databind.ObjectMapper;
import gbm.models.BIFolder;

import org.apache.commons.collections.TreeBag;
import org.apache.commons.configuration.PropertiesConfiguration;
import org.apache.commons.configuration.PropertiesConfigurationLayout;
import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.client.HttpClient;
import org.apache.http.client.config.RequestConfig;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.client.methods.HttpUriRequest;
import org.apache.http.entity.ContentType;
import org.apache.http.entity.StringEntity;
import org.apache.http.entity.mime.MultipartEntity;
import org.apache.http.entity.mime.content.InputStreamBody;
import org.apache.http.impl.client.HttpClientBuilder;
import org.apache.http.util.EntityUtils;
import org.apache.log4j.Logger;
import org.json.simple.JSONArray;
import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;
import static com.alation.tableau.utils.Constants.BI_SERVER_TITLE;
import static com.alation.tableau.utils.TableauConstants.ALATION_GBM_V2_URL;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileWriter;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.UnsupportedEncodingException;
import java.util.HashMap;
import java.util.Map;
import java.util.Set;
import java.util.TreeMap;

/**
 * Class designed to automate GBM / BI Data processing with Alation.
 * <p>
 * Developed in early April 2019 - against weak BI Bulk Metadata API processor.
 * Main intention for this class to hide all the complexities doing CRUD
 * operations against the BI Data sources (trying to overcome all BULK
 * processing!!).
 *
 * @author babu.balasubramanian
 */
public class MainManager {

    public static final String BI_SOURCE_ID = "bi.source.id"; // Alation Id which points to BI
    public static final String BI_FACTORY_PROVIDER = "bi.base.factory"; // Base factory provider
    // Contains Target BI Server
    public static final String BI_BASE_SOURCE_URL = "bi.base.url";
    public static final String BI_API_URL = "bi.api.url";
    public static final String BI_TOKEN = "bi.token";	
    public static final String REMOVE_OLD = "remove.old";
    //Data Source ID
    public static final String DATA_SOURCE_ID = "data.source.id";
    public static final String CLIENT_ID = "bi.client.id";
    public static final String CLIENT_SECRET = "bi.client.secret";
    // QlikView Details
    public static final String BI_USERNAME = "bi.username";
    public static final String BI_PASSWORD = "bi.password";
    public static final String WEBDRIVER_PATH = "webdriver.path";
    public static final String ENABLE_HEADLESS_BROWSER = "enable.headless.browser";
    public static final String BROWSER_DRIVER = "browser.driver";
    public static final String BROWSER_ZOOM_LEVEL = "qlik.zoom";
    public static final String LOAD_IMAGES = "bi.load.images";
    public static final String BI_INCLUDE_FOLDERS = "bi.include.folders";
    public static final String BI_EXCLUDE_FOLDERS = "bi.exclude.folders";
    public static final String CONTENT_URL="bi.content.url";
    // Static Stuff
    private static final Logger logger = Logger.getLogger(MainManager.class);
    // Instance
    public static GBMConfig config;

    private static GBMObjectStore gbmObjectStore = GBMObjectStore.getStore();

   
    // Utility

    private boolean isQvd = false;

    /*
     * This class requires following parameter configured in properties (runtime) in
     * order to successfully run this processor.
     *
     * alation.base.url auth.token bi.source.id
     *
     */

    public static void main(String[] args) {
        logger.info("STARTS");

        MainManager syncManager = new MainManager();
        syncManager.startProcess();

        logger.info("END");
    }

    /**
     * Verifies the Alation Server Connectivity and Alation token.
     *
     * @throws Exception
     */
    public static void verifyAlationServerConnectivity() throws Exception {
        HttpResponse response;
        logger.info("Verifying Alation Server Connectivity");

        response = APIUtils.doGET("/catalog/datasource/?limit=2");

        try {
            String responseString = APIUtils.convert(response.getEntity());
            if (response.getStatusLine().getStatusCode() != 200) {
                logger.error("Error Code ::: " + response.getStatusLine().getStatusCode());
                logger.error("Response === " + responseString);
                logger.error("Response === " + new ObjectMapper().writeValueAsString(response.getStatusLine()));
                throw new Exception(responseString);
            } else {
                logger.info("Alation Connectivity successful");
            }
        } catch (Exception ex) {
            throw ex;
        }
    }

    private void startProcess() {
        try {
            PropHelper propertyHelper = PropHelper.getHelper();
            config = new GBMConfig(propertyHelper.getBaseURL(), propertyHelper.getToken(),
                    propertyHelper.getProperties().getProperty(BI_SOURCE_ID));

            config.setBiBaseURL(propertyHelper.getProperties().getProperty(BI_BASE_SOURCE_URL));
            config.setBiToken(propertyHelper.getProperties().getProperty(BI_TOKEN));
            config.setProperties(propertyHelper.getProperties());
            config.setBiApiURL(propertyHelper.getProperties().getProperty(BI_API_URL));
            if (propertyHelper.getProperties().getProperty(DATA_SOURCE_ID) != null) {
                config.setDataSourceId(propertyHelper.getProperties().getProperty(DATA_SOURCE_ID));
            }
            config.setBiUsername(propertyHelper.getProperties().getProperty(BI_USERNAME));
            config.setBiPassword(propertyHelper.getProperties().getProperty(BI_PASSWORD));
            config.setEnableHeadlessBrowser(propertyHelper.getProperties().getProperty(ENABLE_HEADLESS_BROWSER));
            config.setLoadImages(propertyHelper.getProperties().getProperty(LOAD_IMAGES));
            if (propertyHelper.getProperties().getProperty(BROWSER_ZOOM_LEVEL) != null) {
                config.setBrowserZoomLevel(Integer.parseInt(propertyHelper.getProperties().getProperty(BROWSER_ZOOM_LEVEL)));
            }
            config.setBrowserDriver(propertyHelper.getProperties().getProperty(BROWSER_DRIVER));
            config.setIncludeFolders(propertyHelper.getProperties().getProperty(BI_INCLUDE_FOLDERS));
            config.setExcludeFolders(propertyHelper.getProperties().getProperty(BI_EXCLUDE_FOLDERS));
            config.setLoadImages(propertyHelper.getProperties().getProperty(LOAD_IMAGES));
            config.setContentUrl(propertyHelper.getProperties().getProperty(CONTENT_URL));
            logger.info(config);

            //verify Alation server properties
            verifyAlationServerConnectivity();
            verifyTableauConnectivity();
            //Create new BI server if server is not provided in property
            createBIVirtualServer();
            //Start process
            runProcess();
        } catch (Exception e) {
            logger.error("Failed startProcess()", e);
        }
    }

    /**
     * Initialize setup providers, store the data in temp, and sync the data
     *
     * @throws Exception
     */
    private void runProcess() throws Exception {

    	TableauFolderProvider tableauFolderProvider = new TableauFolderProvider();
        Set<BIFolder> folders = tableauFolderProvider.getAllFolders();
        logger.info("folders size:" + folders.size());

        for (BIFolder folder : folders) {
            extractMetaDataForFolder(folder, tableauFolderProvider);
        }
        TableauDatasourceProvider tableauDatasourceProvider = new TableauDatasourceProvider();
        tableauDatasourceProvider.getDatasources();
		
		 if(!folders.isEmpty()) { postPreviewImage(); }
		 
        
      logger.info("Process Completed::::");

    }
    

    private void extractMetaDataForFolder(BIFolder folder, TableauFolderProvider tableauFolderProvider) {

        logger.info("Extracting info for the project === " + folder.getName());

        tableauFolderProvider.passFolder(folder);
        tableauFolderProvider.getFolders();
        GBMObjectStore.getStore().put(TableauConstants.CURRENT_FOLDER, folder);

		/*
		 * TableauDatasourceProvider tableauDatasourceProvider = new
		 * TableauDatasourceProvider(); tableauDatasourceProvider.getDatasources();
		 */

/*
        TableauConnectionProvider connectionProvider = new TableauConnectionProvider();
        connectionProvider.getConnections();


        TableauDatasourceColumnProvider tableauDatasourceColumnProvider = new TableauDatasourceColumnProvider();
        tableauDatasourceColumnProvider.getDatasourceColumns();

    


        TableauReportColumnProvider reportColumnProvider = new TableauReportColumnProvider();

        reportColumnProvider.getReportColumns();
*/
        
        TableauReportProvider reportProvider = new TableauReportProvider();
        reportProvider.getReports();

    }
    
    private void postPreviewImage() {
		try {
			HttpResponse response = ApiUtils.doGET(ALATION_GBM_V2_URL+config.getBiId()+"/report");
			
			Map<String, InputStream> existingDataMap = (HashMap<String, InputStream>) gbmObjectStore.get("previewImage");
			TreeMap<String, Integer> reportDataMap = new TreeMap<String, Integer>();
			if (response.getStatusLine().getStatusCode() == 200) {
				org.json.JSONArray jsonArray = TableauUtil.parseResponseToJsonArray(response);
				for (Object viewObj : jsonArray) {
					org.json.JSONObject viewJson = (org.json.JSONObject) viewObj;
					Integer reportId = (Integer) viewJson.get("id");
					String externalId = (String) viewJson.get("external_id");
					reportDataMap.put(externalId, reportId);					
				}				
			}
			if(!reportDataMap.isEmpty() && !existingDataMap.isEmpty()) {
				for(String tableauReportId : existingDataMap.keySet()) {
					if(reportDataMap.get(tableauReportId) != null) {
						Integer alationReportId = reportDataMap.get(tableauReportId);
						String generateUrl = ALATION_GBM_V2_URL + config.getBiId() + "/report/"+alationReportId+"/image/";
						MultipartEntity entity = new MultipartEntity();
						entity.addPart("file", new InputStreamBody(existingDataMap.get(tableauReportId), "test"+alationReportId));
						
						logger.info(generateUrl);
						HttpResponse responseImage  = ApiUtils.doPOST(entity, generateUrl);
					}
				}
				logger.info("Completed posting image");
			}
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
  
    private static void verifyTableauConnectivity() throws  Exception {
		logger.info("Verifying Tableau connectivity and sign in");
        JSONObject jsonObject = new JSONObject();
        JSONObject userjsonObject = new JSONObject();
        JSONObject sitejsonObject = new JSONObject();
        userjsonObject.put("name", config.getBiUsername());
        userjsonObject.put("password", config.getBiPassword());
        sitejsonObject.put("contentUrl", config.getContentUrl());
        userjsonObject.put("site", sitejsonObject);
        jsonObject.put("credentials", userjsonObject);
        HttpResponse response = ApiUtils.doPOST(config.getBiBaseURL(), new StringEntity(jsonObject.toString()), TableauConstants.API_ID+"auth/signin");
        logger.info("response.getStatusLine().getStatusCode()>>>>"+response.getStatusLine().getStatusCode());        
        if (response.getStatusLine().getStatusCode() == 200) {
            String responseString = EntityUtils.toString(response.getEntity());
            logger.info("responseString>>>>"+responseString);   
			 JSONParser jsonParser = new JSONParser();
			 Object object = jsonParser.parse(responseString); 
			 JSONObject responseObject = (JSONObject) object; 
			 JSONObject getCredentialsDetails = (JSONObject) responseObject.get("credentials");
			 JSONObject getSiteInfo = (JSONObject) getCredentialsDetails.get("site");
			 Object siteId = (Object) getSiteInfo.get("id");
			 gbmObjectStore.put(Constants.SITE_ID, siteId); 
			 Object tokenId = (Object) getCredentialsDetails.get(Constants.ACCESS_TOKEN);
			 gbmObjectStore.put(Constants.ACCESS_TOKEN, tokenId); 			 
        } else {
            String responseString = EntityUtils.toString(response.getEntity());
            JSONParser jsonParser = new JSONParser();
            Object object = jsonParser.parse(responseString);
            JSONObject responseObject = (JSONObject) object;
            logger.fatal(responseObject.toString());
            throw new IllegalArgumentException("Could not Signin BI Server, Please check the error message");
        }
	}
    
    private void createBIVirtualServer() throws Exception {
        if (config.getBiId() == null || config.getBiId().isEmpty()) {
            JSONArray jsonArray = new JSONArray();
            JSONObject jsonObject = new JSONObject();
            jsonObject.put("uri", config.getBiBaseURL());
            jsonObject.put("title", PropHelper.getHelper().getProperties().getProperty(BI_SERVER_TITLE));
            jsonArray.add(jsonObject);
            logger.info("Creating BI Server: " + jsonArray.toString());
            HttpResponse response = APIUtils.doPOST(new StringEntity(jsonArray.toString()), ALATION_GBM_V2_URL);
            if (response.getStatusLine().getStatusCode() == 200) {
                String responseString = EntityUtils.toString(response.getEntity());
                JSONParser jsonParser = new JSONParser();
                Object object = jsonParser.parse(responseString);
                JSONObject responseObject = (JSONObject) object;
                JSONArray biIds = (JSONArray) responseObject.get("Server IDs");
                config.setBiId(biIds.get(0).toString());
                logger.info("Created a BI server with ID: " + config.getBiId());
                updatePropertiesFileWithBiServerId(config.getBiId());
            } else {
                String responseString = EntityUtils.toString(response.getEntity());
                JSONParser jsonParser = new JSONParser();
                Object object = jsonParser.parse(responseString);
                JSONObject responseObject = (JSONObject) object;
                logger.fatal(responseObject.toString());
                throw new IllegalArgumentException("Could not create BI Server, Please check the error message");
            }
        }
    }
    
    private void updatePropertiesFileWithBiServerId(String biServerId) {
        logger.info("Updating properties file with BI server id");
        try {
            PropertiesConfiguration propertiesConfiguration = new PropertiesConfiguration();
            PropertiesConfigurationLayout layout = new PropertiesConfigurationLayout(propertiesConfiguration);
            layout.load(new InputStreamReader(new FileInputStream(System.getProperty("config"))));

            propertiesConfiguration.setProperty(BI_SOURCE_ID, biServerId);
            layout.save(new FileWriter(System.getProperty("config"), false));
        } catch (Exception ex) {
            logger.error("Error while updating biServer id to properties file");
            logger.error(ex.getMessage(), ex);
        }
    }
}
